///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Midterm - Loops
///
/// @file    main.c
/// @version 1.0
///
/// Sums up values in arrays
///
/// Your task is to print out the sums of the 3 arrays...
///
/// The three arrays are held in a structure.  Consult numbers.h for the details.
///
/// For array1[], you'll iterate over using a for() loop.  The correct sum for 
/// array1[] is:  48723737032
/// 
/// For array2[], you'll iterate over it using a while()... loop.
///
/// For array3[], you'll iterate over it using a do ... while() loop.
///
/// Sample output:  
/// $ ./main
/// 11111111111
/// 22222222222
/// 33333333333
/// $
///
/// @brief Midterm - Loops - EE 205 - Spr 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "numbers.h"

int main() {

   extern struct ThreeArraysStruct threeArrays;

   int i1,i2,i3;
   long array1sum=0;
   long array2sum=0;
   long array3sum=0;
   long arraysum=0;

   for (i1=0;i1<100;i1++){
      array1sum=array1sum + threeArrays.array1[i1];
   }
   i2=0;
    while (i2<100){
      array2sum=array2sum + threeArrays.array2[i2++];
    }
   i3=0;
    do{
       array3sum=array3sum + threeArrays.array3[i3++];
    }while(i3<=100);
   
    arraysum=array1sum+array2sum+array3sum;



     printf("Array 1 Sum = %lu\n",array1sum);
     printf("Array 2 Sum = %lu\n",array2sum);
    printf("Array 3 Sum = %lu\n",array3sum);
   printf("Array Sums = %lu\n",arraysum);

      
   // Sum array1[] with a for() loop and print the result

   // Sum array2[] with a while() { } loop and print the result


   // Sum array3[] with a do { } while() loop and print the result


	return 0;
}

